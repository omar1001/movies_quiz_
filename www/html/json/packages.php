<?php

	if(session_status() != PHP_SESSION_ACTIVE) session_start();

	if(!isset($_SESSION['ADMIN_AUTHENTICATED']) || !isset($_SESSION['publisher'])) {
		echo "database_error";
		exit();
	}

	$result;
	$hostname = 'localhost';
	
	$conn = mysqli_connect($hostname, 'test', '1');
	if(!$conn){
		echo "database_error";
		exit();
	}

	$query = "SELECT id, points, clips, color, published FROM movies_quiz.packages ORDER BY id DESC";
	mysqli_query($conn, "SET NAMES 'utf8'");

	$result = mysqli_query($conn, $query);
	if (!$result) die('database_error');

	
	$json_array = array();
	while($row = mysqli_fetch_assoc($result)) {
		$json_array[] = $row;
	}
	

	echo json_encode($json_array, JSON_UNESCAPED_UNICODE);


?>
