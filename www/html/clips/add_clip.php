<?php
	session_start();
	if(!isset($_SESSION['ADMIN_AUTHENTICATED'])) {
		echo "login";
		exit();
	}

	//echo($_POST['clip_json']);

	$conn = mysqli_connect('localhost', 'test', '1');
	if($conn === FALSE) {
		echo "database_error";
		exit();
	}

	$clip_json = json_decode($_POST['clip_json'], true);

	$query = "INSERT INTO movies_quiz.clips (clip, movie_id) VALUES('".mysqli_real_escape_string($conn, $clip_json['clip'])."', '".mysqli_real_escape_string($conn, $clip_json['answer'])."')";

	mysqli_query($conn, "SET NAMES 'utf8'");

	$result = mysqli_query($conn, $query);
	if($result) {
		$clip_id = mysqli_insert_id($conn);
		foreach($clip_json['choices'] as $choice) {
			$query = "INSERT INTO movies_quiz.movies_clips_choices (clip_id, movie_id) VALUES('".$clip_id."', '".mysqli_real_escape_string($conn, $choice)."')";
			if(! mysqli_query($conn, $query)) {
				exit();
			}
		}
		echo 'done';

	} else {
		echo('duplicate');
	}


?>
