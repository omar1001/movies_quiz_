<?php
	session_start();
	if(!isset($_SESSION['ADMIN_AUTHENTICATED']) || !isset($_SESSION['publisher'])) {
		header('Location: ../home.php');
		exit();
	}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>movies Quiz</title>

		<link href="../css/bootstrap.css" rel="stylesheet">
		<link href="../css/select2.min.css" rel="stylesheet">

		<script src="../js/jquery.min.js"></script>
		<script src="../js/jscolor.min.js"></script>
		<script src="../js/tableheadfixer.min.js"></script>
		<script src="../js/select2.min.js"></script>

		<script src="../js/bootstrap.min.js"></script>
		<script src="../js/publish_main.js"></script>
		
		
    </head>
	
    <body >

	<div class="container-fluid">
		<div class="row" style="padding:25px;">
			<div class="col-md-4">
				<h3 class="text-center text-primary">
					Package
				</h3>
				<form role="form">
					<div class="form-group">
						<label for="clips_number_entry" >
							Clips Number <label ><h6 id="unpackaged_clips_number"></h6></label>
						</label>
						
						<input type="text" class="form-control" id="clips_number_entry" />
					</div>
					<div class="form-group">
						 
						<label for="points_entry">
							points
						</label>
						<input type="text" class="form-control" id="points_entry" />
					</div>
					<div class="form-group">
						 
						<label for="color_entry">
							Color
						</label>
						<input type="text" class="form-control jscolor{position:'top'}" id="color_entry" />
					</div>
					<div style="text-align: center;">
						<button type="button" class="btn btn-default"  onclick="submit_package();">
							Submit
						</button>
					</div>
				</form>
			</div>
			<div class="col-md-8">
				<h3 class="text-center text-primary">
					Packages List <label><h5 id="total_packages_number"></h5></label>
				</h3>
				<div style=" width:100%;display: block; height: 300px;overflow-y: auto;" id="packages_list_div">
				<table class="table table-condensed table-hover table-bordered" id="packages_table">
					<thead>
						<tr >
							<th style="text-align: center;">
								id
							</th>
							<th style="text-align: center;">
								points
							</th>
							<th style="text-align: center;">
								clips
							</th>
							<th style="text-align: center;">
								color
							</th>
							<th style="text-align: center;">
								status
							</th>
						</tr>
					</thead>
					<tbody id="packages_table_body" style="width: 100%; height: 10px; overflow-y: auto; ">

					</tbody>
				</table>
				</div>

			</div>
		</div>

		<hr>

		<div class="row" style="padding:25px; direction: rtl;">
			<div class="col-md-8" >
				<h3 class="text-center text-primary">
					<label><h5 id="total_clips_number"></h5></label> Clips 
				</h3>
				<div style=" height: 500px; overflow: auto; " id="clips_table_div">
					<table class="table table-condensed table-hover table-bordered"  id="clips_table">
						<thead>
							<tr>
								<th style="text-align: center;">
									id
								</th>
								<th style="text-align: center;">
									clip
								</th>
								<th style="text-align: center;">
									answer
								</th>
								<th style="text-align: center;">
									choices
								</th>
								<th style="text-align: center;">
									delete
								</th>
							</tr>
						</thead>
						<tbody id="clips_table_body">

						</tbody>
					</table>
				</div>
			</div>


			<div class="col-md-4"  >
				<h3 class="text-center text-primary">
					<label><h5 id="movies_number"></h5></label> Movies
				</h3>
				<div style="height: 500px; overflow: auto;" id="movies_table_div">
					<table class="table table-hover table-condensed table-bordered" id="movies_table">
						<thead>
							<tr>
								<th class="text-center">
									id
								</th>
								<th class="text-center">
									name
								</th>
								<th class="text-center">
									ans occ
								</th>
								<th class="text-center">
									chcs occ
								</th>
								<th class="text-center">
									del
								</th>
							</tr>
						</thead>
						<tbody id="movies_table_body">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


    </body>

    <script type="text/javascript">
    	update_packages_list();
    	$("#clips_table").tableHeadFixer();
    	$("#packages_table").tableHeadFixer();
    	$("#movies_table").tableHeadFixer();

		$("#packages_list_div").on( 'mousewheel', function ( e ) {
		    var event = e.originalEvent,
		        d = event.wheelDelta || -event.detail;
		    
		    this.scrollTop += ( d < 0 ? 1 : -1 ) * 30;
		    e.preventDefault();
		});
		$("#movies_table_div").on( 'mousewheel', function ( e ) {
		    var event = e.originalEvent,
		        d = event.wheelDelta || -event.detail;
		    
		    this.scrollTop += ( d < 0 ? 1 : -1 ) * 30;
		    e.preventDefault();
		});
		$("#clips_table_div").on( 'mousewheel', function ( e ) {
		    var event = e.originalEvent,
		        d = event.wheelDelta || -event.detail;
		    
		    this.scrollTop += ( d < 0 ? 1 : -1 ) * 30;
		    e.preventDefault();
		});
    </script>


</html>