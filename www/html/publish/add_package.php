<?php

	if(session_status() != PHP_SESSION_ACTIVE) session_start();

	if(!isset($_SESSION['ADMIN_AUTHENTICATED']) || !isset($_SESSION['publisher'])) {
		echo "re-login";
		exit();
	}
	if(!isset($_POST['entries'])) {
		echo "entries missing";
		exit();
	}

	
	$entries = json_decode($_POST['entries'], true);

	//check if every entry is submitted.
	if(!isset($entries['points']) || !isset($entries['color']) || !isset($entries['clips'])) {
		die("entries not completed");
	}

	$conn = mysqli_connect('localhost', 'test', '1');
	if($conn === FALSE) {
		echo "database connection";
		exit();
	}


	$query = "INSERT INTO movies_quiz.packages (points, color, clips) VALUES(" . $entries['points'] . ", '" . $entries['color'] . "', " . count($entries['clips']) . ")";

	mysqli_query($conn, "SET NAMES 'utf8'");

	$result = mysqli_query($conn, $query);
	if($result) {
		$package_id = mysqli_insert_id($conn);
		$queries = "";
		foreach($entries['clips'] as $value) {
			$queries .= "INSERT INTO movies_quiz.packages_clips (package_id, clip_id) VALUES(" . $package_id . ", " . $value['clip_id'] . ");";
		}
		mysqli_multi_query($conn, $queries);

		do {
		    if($result = mysqli_store_result($conn)){
		        mysqli_free_result($result);
		    }
		} while(mysqli_next_result($conn));

		if(mysqli_error($conn)) {
			mysqli_query($conn, "DELETE FROM movies_quiz.packages WHERE id = '".$package_id."'");
		    die("Couldn't insert some or all packages_clips" . mysqli_error($conn));
		} else {
			echo "done";
		}

	} else {
		echo('Error in inserting packages.');
	}
?>