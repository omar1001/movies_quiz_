<?php

	if(session_status() != PHP_SESSION_ACTIVE) session_start();

	if(!isset($_SESSION['ADMIN_AUTHENTICATED']) || !isset($_SESSION['publisher'])) {
		echo "re-login";
		exit();
	}
	if(!isset($_POST['id'])) {
		echo "id missing";
		exit();
	}

	
	$id = $_POST['id'];


	$conn = mysqli_connect('localhost', 'test', '1');
	if($conn === FALSE) {
		echo "database connection";
		exit();
	}
	

	mysqli_query($conn, "SET NAMES 'utf8'");

	mysqli_query($conn, "SET @packages_number = (SELECT MAX(number) FROM movies_quiz.packages) + 1");

	$query = "UPDATE movies_quiz.packages SET published = 1, number = @packages_number WHERE id = " 
				. mysqli_real_escape_string($conn, $id);

	$result = mysqli_query($conn, $query);
	if($result) {
		mysqli_query($conn, "SET @movies_clips_choices_number = (SELECT MAX(number) FROM movies_quiz.movies_clips_choices) + 1");
		mysqli_query($conn, "SET @packages_clips_number = (SELECT MAX(number) FROM movies_quiz.packages_clips) + 1");
		mysqli_query($conn, "SET @clips_number = (SELECT MAX(number) FROM movies_quiz.clips) + 1");
		mysqli_query($conn, "SET @movies_number = (SELECT MAX(number) FROM movies_quiz.movies) + 1");
		mysqli_select_db($conn, "movies_quiz");
		$result = mysqli_query($conn, "
				UPDATE movies
					JOIN movies_clips_choices 
						ON movies.id = movies_clips_choices.movie_id  
					JOIN clips 
						ON clips.id = movies_clips_choices.clip_id  
					JOIN packages_clips 
						ON packages_clips.clip_id = clips.id  
					JOIN packages 
						ON packages.id = packages_clips.package_id 
				SET 
					clips.number = @clips_number,
					movies.number = 
						CASE
							WHEN movies.number = -1 
								THEN @movies_number 
							ELSE movies.number
						END,
					movies_clips_choices.number = @movies_clips_choices_number,
					packages_clips.number = @packages_clips_number
				WHERE 
						packages.id = ".mysqli_real_escape_string($conn, $id)."
			");


		if($result) {
			echo "done";
		} else {
			echo("couldn't set tables numbers: " . mysqli_error($conn));
		}
	} else {
		echo("couldn't publish this package: " . mysqli_error($conn));
	}
?>
