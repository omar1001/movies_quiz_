<?php
	session_start();
	if(!isset($_SESSION['ADMIN_AUTHENTICATED'])) {
		header('Location: .');
		exit();
	}


?>


<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>movies Quiz</title>

		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/select2.min.css" rel="stylesheet">

		<script src="js/jquery.min.js"></script>
		<script src="js/select2.min.js"></script>
		<script src="js/tableheadfixer.min.js"></script>

		<script src="js/bootstrap.min.js"></script>
		
		
    </head>
	
    <body >

    <div style="text-align:right; margin-top: 10px; margin-right: 10px">
        <?php
	    	if(isset($_SESSION['publisher'])) {
	    		echo "<a href=\"publish\" >Publish</a><br>";

	    	}
	    ?>
		<a href="logout.php" >Logout</a>
	</div>
<div class="container-fluid" >

	<div class="row" style="margin-top: 10px; margin-bottom: 45px; margin-left: 1px; margin-right: 1px; border:3px solid grey; padding:20px; ">
		<div class="col-md-4 " style="margin-top: 10px">
			<div class="row ">
				<div class="col-md-2">
					<h4>
						Clip:
					</h4>
				</div>
				<div class="col-md-10 ">
					 
					<textarea class="form-control"  style="width: 100%; height:100px; text-align:right;" style="text-align:right; font-size: 20px;" id="clip_entry" onfocus="trim_text_area(this)">
					</textarea>
					
				</div>
			</div>
		</div>
		<div class="col-md-8" style="margin-top: 15px">
			<div class="row">
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-4">
							<h4>
								Answer:
							</h4>
						</div>
						<div class="col-md-8">
							 
							<select  multiple id="answer_select2" data-dir="rtl" style="width:100%;">
								
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-2">
							<h4>
								Choices:
							</h4>
						</div>
						<div class="col-md-10">
							 
							<select  multiple id="choices_select2" data-dir="rtl" style="width:100%;">
								
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="row" style="margin-bottom: 10px;">
			<div class="col-md-12 text-center">
				 
				<button type="button" class="btn btn-lg btn-success " style="margin-top: 20px" onclick="submit_clip()" >
					Add Clip
				</button>
			</div>
		</div>
	</div>

	<hr>

	<div class="row "  style="margin-top: 35px; margin-bottom: 45px;  margin-left: 1px; margin-right: 1px; border:3px solid grey; padding:20px;" >
		<div class="col-md-6">
			<div class="row" style="margin-top: 25px">
				<div class="col-md-2">
					<h4>
						Movie:
					</h4>
				</div>
				<div class="col-md-7" style="margin-bottom: 20px">
					 
					<textarea class="form-control "  rows="3" cols="40" style="text-align:right; width:100%;" id="movie_entry" onfocus="trim_text_area(this)">
						
					</textarea>
				</div>
				<div class="col-md-3 text-center" style="margin-top: 20px">
					 
					<button type="button" class="btn	  btn-success" onclick="add_movie()">
						Add Movie
					</button>
				</div>
			</div>
		</div>
		<div class="col-md-6" style="direction: rtl;">
			
			<h4 class="text-center">
				<button type="button" class="btn btn-xs center" onclick="toggle_movies_table()">
					view/hide
				</button>
				Movies

			</h4>

			<div id="movies_table_div" >
			<div style="display: block; height: 400px;overflow-y: auto; margin-bottom:20px" id="movies_table_div2" >
				<table class="table table-hover table-condensed table-bordered" id='movies_table'>
					<thead>
						<tr>
							<th style="text-align: center;">
								id
							</th>
							<th style="text-align: center;">
								name
							</th>
						</tr>
					</thead>
					<tbody id="movies_table_body">
						
					</tbody>
				</table>
			</div>
			</div>
		</div>
	</div>

	<hr>
	<div class="row" style="margin-top: 45px; margin-bottom: 15px; margin-left: 1px; margin-right: 1px; border:3px solid grey; padding:20px;">
		<div class="col-md-12" style="direction: rtl;">
			<h3 class="text-center">
				Clips
			</h3>
			<div style=" width:100%;display: block; height: 500px;overflow-y: auto;" id="clips_table_div">
				<table class="table table-hover table-bordered table-condensed"  id="clips_table">
					<thead>
						<tr>
							<th style="text-align: center;">
								id
							</th>
							<th style="text-align: center;">
								clip
							</th>
							<th style="text-align: center;">
								answer
							</th>
							<th style="text-align: center;">
								choices
							</th>
						</tr>
					</thead>
					<tbody id="clips_table_body">

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>








    </body>

    <script src="/js/home_main.js"></script>
    <script>
    function toggle_movies_table() {
    	var movies_table_display = document.getElementById("movies_table_div").style.display;
    	document.getElementById("movies_table_div").style.display = movies_table_display == "none"? "inherit": "none";
    }
    $("#movies_table").tableHeadFixer();
    $("#clips_table").tableHeadFixer();


	$("#clips_table_div").on( 'mousewheel', function ( e ) {
	    var event = e.originalEvent,
	        d = event.wheelDelta || -event.detail;
	    
	    this.scrollTop += ( d < 0 ? 1 : -1 ) * 30;
	    e.preventDefault();
	});
	$("#movies_table_div2").on( 'mousewheel', function ( e ) {
	    var event = e.originalEvent,
	        d = event.wheelDelta || -event.detail;
	    
	    this.scrollTop += ( d < 0 ? 1 : -1 ) * 30;
	    e.preventDefault();
	});

    </script>
</html>