var movies;
var clips_choices;
var clips;


function submit_clip() {
	var choices = new Array(); choices = $("#choices_select2").val();
	var choices_length = Object.keys(choices).length;
	if(choices_length == 0) {
		alert("Choices # must not be 0");
		return;
	}
	var answer = $("#answer_select2").val()[0];
	var answer_in_choices = false;
	for(var i=0; i<choices_length; i++) {
		if(answer == choices[i]) {
			answer_in_choices = true;
			break;
		}
	}
	if(!answer_in_choices) {
		alert("Answer is not in choices");
		return;
	}
	console.log(choices);
	if(choices_length < 4) {
		choices = choices.concat(get_random_movies(3, [answer]));
	}

	var clip = document.getElementById('clip_entry').value.trim();
	var clip_length = Object.keys(clip).length;
	if(clip_length < 4) {
		alert("Clip is too short");
		return;
	}

	var json_array = new Array();
	json_array = {};
	json_array['clip'] = clip;
	json_array['answer'] = answer;
	json_array['choices'] = clone(choices);

	var json = JSON.stringify(json_array);


	//submit clip json to server
	$.ajax({
		type: "POST",
		url: "/clips/add_clip.php",
		data: "clip_json="+json,
		cache: false,
		success: function(respond) {
			if(respond === "database_error" || respond === "")
				alert('error; ' + respond);
			else if (respond === "enter_movie")
				alert('error; ' + respond);
			else if (respond === 'duplicate')
				alert('this clip exists');
			else if (respond === "done") {
				clean_clips_entries();
				update_clips();
			}
		}
	});
}


function update_clips() {

	/*
		fetch clips json 
		then fetch clips choices after clips is successfully fetched
		then update after choices is successfully fetched
	*/
	$.ajax({
		type: "POST",
		url: "/json/clips.php",
		data: "",
		cache: false,
		success: function(json) {
			console.log(json);
			if(json === "database_error" || json === "")
				alert('can\'t fetch clips; ' + json);
			else if(json != "[]"){
				try {
					clips = JSON.parse(json);
					get_clips_choices();
				} catch (e) {
					alert("Couldn't fetch clips");
					console.log(e);
				}
			}
			
		}
	});

	function get_clips_choices() {
		$.ajax({
			type: "POST",
			url: "/json/clips_choices.php",
			data: "",
			cache: false,
			success: function(json) {
				console.log(json);
				if(json === "database_error" || json === "")
					alert('can\'t fetch clips choices; ' + json);
				else if(json != "[]"){
					try{
						clips_choices = JSON.parse(json);
						update();
					} catch (e) {
						alert("Couldn't fetch clips_choices");
						console.log(e);
					}
				}
			}
		});
	}

	function update() {
		var html = ""; var id; var name;
		var table_html = "";
		for(var i=0; i<clips.length; i++) {
			var id = clips[i].id;
			var clip = clips[i].clip;
			var answer = get_movie_by_id(clips[i].movie_id);
			table_html += "<tr>"+
				"<td style='text-align: center;'>" +
				id +
				"</td>" +
				"<td style='text-align: center;'>" +
				clip +
				"</td>" +
				"<td style='text-align: center;'>(" +
				answer +
				")</td>" +
				"<td style='text-align: center;'>";
			var clip_choices = get_choices_by_clip_id(id);
			var len = clip_choices.length;
			for(var c=0; c<len; c++) 
				table_html += "(" + get_movie_by_id(clip_choices[c].movie_id) + ")" + (c==len-1? "": "-");
			table_html += "</td>";
			table_html += "</tr>";
		}
		document.getElementById('clips_table_body').innerHTML = table_html;
	}
}


function get_choices_by_clip_id(id) {
	var ans = [];
	for(var i in clips_choices) {
		if(clips_choices[i].clip_id == id) {
			ans.push({
				movie_id: clips_choices[i].movie_id
			});
		}
	}
	return ans;
}

function get_movie_by_id(id) {
	for(var i=0; i<movies.length; i++) {
		if(movies[i].id == id) return movies[i].name;
	}
}


function clean_clips_entries() {
	document.getElementById('clip_entry').value = '';
	$("#choices_select2").select2("val", "All");
	$("#answer_select2").select2("val", "All");
}

function add_movie() {
	var movie_name = document.getElementById("movie_entry").value.trim();
	if(movie_name == "" || movie_name.length <4) return;
	$.ajax({
		type: "POST",
		url: "/movies/add_movie.php",
		data: "movie_name="+movie_name,
		cache: false,
		success: function(respond) {
			console.log("response: " + respond);
			if(respond === "database_error" || respond === "")
				alert('error; ' + respond);
			else if (respond === "enter_movie")
				alert('error; ' + respond);
			else if (respond === 'duplicate')
				alert('this movie exists');
			else if (respond === "done") {
				update_movies();
				document.getElementById("movie_entry").value = "";
			}
		}
	});	
}

$("#answer_select2").select2({
  	maximumSelectionLength: 1,
		"language": {
		"noResults": function(){
				return "No Movies..";
		},
		maximumSelected: function(args) {
			// args.maximum is the maximum number of items the user may select
			return "The Answer is only 1";
		}
}}).on('select2:focus', select2Focus).on("select2:blur", function () {
    $(this).one('select2:focus', select2Focus)
}).on("select2:select", function(e) {
	console.log(e.currentTarget.value);
	$("#choices_select2").val(e.currentTarget.value).trigger("change");
	$("#choices_select2").focus();
});

$("#choices_select2").select2({
  	maximumSelectionLength: 4,
		"language": {
		"noResults": function(){
				return "No Movies..";
		},
		maximumSelected: function(args) {
			// args.maximum is the maximum number of items the user may select
			return "maximum 4 movies";
		}
}}).on('select2:focus', select2Focus).on("select2:blur", function () {
    $(this).one('select2:focus', select2Focus)
}).on("select2:select", function(e) {
	//maybe returns movie occurence
});
	

function select2Focus() {
    var select2 = $(this).data('select2');
    setTimeout(function() {
        //if (!select2.opened()) {
            select2.open();
        //}
    }, 0);
}

//fetch movies
function update_movies() {
	$.ajax({
		type: "POST",
		url: "/json/movies.php",
		data: "",
		cache: false,
		success: function(json) {
			console.log(json);
			if(json === "database_error" || json === "")
				alert('can\'t fetch movies; ' + json);
			else {
					movies = JSON.parse(json);
					var html = ""; var id; var name;
					var table_html = "";
					for(var i=0; i<movies.length; i++) {
						id = movies[i].id;
						name = movies[i].name;
						html += '<option  value="'+id+'">'  + name + '</option>';
						table_html += "<tr>"+
							"<td style='text-align: center;'>" +
							id +
							"</td>" +
							"<td style='text-align: center;'>" +
							name +
							"</td>" +
						"</tr>";
					}
					document.getElementById('movies_table_body').innerHTML = table_html;
					document.getElementById('answer_select2').innerHTML = html;
					document.getElementById('choices_select2').innerHTML = html;
					update_clips();
			}
		}
	});
}
update_movies();

// Clone Function to copy two objects
function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
	}
	return copy;
}


function trim_text_area(text_area) {
	text_area.value = text_area.value.trim();
}

function enter_listener_text_area() {
	var key = window.event.keyCode;

    // If the user has pressed enter
    if (key == 13) {
        add_movie();
        return false;
    }
    else {
        return true;
    }
}

function get_random_movies(n, excluded) {
	var arr = new Array();
	while(true) {
		var ran_ind = Math.floor(Math.random()*movies.length);
		var movie_chosen = false;
		for(c in arr) {
			if(arr[c] == movies[ran_ind].id) movie_chosen = true;
		}
		for(c in excluded) {
			if(arr[c] == excluded[c]) movie_chosen = true;
		}
		if(!movie_chosen)
			arr.push(movies[ran_ind].id);
		if(arr.length >= n) break;
	}
	var ans = new Array();
	for(var i=0; i<n ;i++) {
		ans.push(arr[i]);
	}
	return ans;
}