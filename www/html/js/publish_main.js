
var packages;
var packages_clips;
var clips;
var movies;
var clips_choices;
var unpackaged_clips ;
var packaged_clips;


function submit_package() {
	console.log("submit_package");
	toggle_package_submission();

	var clips_number = document.getElementById("clips_number_entry").value;
	var points = document.getElementById("points_entry").value;
	var color = document.getElementById("color_entry").value;

	if(clips_number == "" || points == "" || color == "") {
		console.log("entry missing");
		toggle_package_submission();
		return;
	}

	var clips_ids = select_random_clips(clips_number, []);

	console.log(clips_ids);

	if(clips_ids == false) {
		alert("Not enough unpackaged clips");
		toggle_package_submission();
		return;
	}

	var entries = {
		clips: clips_number,
		points: points,
		color: color,
		clips: clips_ids
	}
	console.log("entries: ");
	console.log(entries);

	var json = JSON.stringify(entries);

	$.ajax({
		type: "POST",
		url: "/publish/add_package.php",
		data: "entries="+json,
		cache: false,
		success: function(respond) {
			console.log("respond: ");
			console.log(respond);
			toggle_package_submission();
			if(respond == "done") {

				document.getElementById("clips_number_entry").value = "";
				document.getElementById("points_entry").value = "";
				document.getElementById("color_entry").value = "";

				// update packages list to get packages list changes 
				// then it will call update clips to update the rest of the data
				update_packages_list();

			} else {
				alert("Server didn't respond with \"done\"");
			}
		}
	});
}


function update_packages_list() {
	console.log("update_packages_list");
	packages = undefined;
	ajax_post(
		"/json/packages.php",
		"",
		function (respond) {
			console.log("Packages: " + respond);
			if(respond == "error") {
				console.log("couldn't fetch packages");
				return;
			}
			packages = JSON.parse(respond);


			//'ptl' stands for 'packages table list' 
			var ptl = "";
			for(var i in packages) {
				ptl += "<tr bgcolor=''>";
				for(var col in packages[i]) {
					ptl += "<td style='text-align:center;' bgcolor='" + (col == 'color'?packages[i][col]:"") + "'>" + 

						(col == 'published'? (
								packages[i][col]=='1'? "published": "<button style='margin-right:10px;' class='btn-success btn-xs btn' onclick=\"publish_package('" + packages[i].id + "')\">publish</button><button class='btn-warning btn btn-xs' onclick=\"delete_package('" + packages[i].id + "')\">delete</button>"
							) : packages[i][col])
							  + "</td>";
				}

				ptl += "</tr>"
			}
			$('#packages_table_body').html(ptl);
			update_clips();
		} 
	);

}

function update_clips() {
	console.log("update_clips");
	//TODO: update unpackaged clips in its label

	var error = false;
	var clips_flag = false;
	var clips_choices_flag = false;
	var movies_flag = false;
	var packages_clips_flag = false;

	unpackaged_clips = undefined;
	clips = undefined;
	clips_choices = undefined;
	packaged_clips = undefined;
	movies = undefined;

	unpackaged_clips = [];
	clips = [];
	clips_choices = [];
	packaged_clips = [];
	movies = [];



	/*
		1_ fetch all clips
			if couldn't then error = true then don't fetch movies & choices.
		2_ fetch clips choices
			if couldn't then error = true then don't fetch movies.
		3_ fetch movies 
			if couldn't then error = true

		after each fetch fetched_flag = true 
			then check to update 
				update if all fetched_flags == true
				this function is called only from update_packages, so it's garenteed that packages are fetched correctly

	*/

	ajax_post(
			"/json/clips.php",
			"",
			function (respond) {
				console.log(respond);
				if(respond == "error") {
					console.log("Can't fetch clips.");
					error = true;
					return;
				}
			    try {

			     	clips = JSON.parse(respond);
			     	clips_flag = true;
			     	
			    } catch (e) {
			    	console.log("Can't convert clips json: " + respond);
			    	error = true;
			        return;
			    }
			    update();
			} 
		);
	if(!error) {
		ajax_post(
			"/json/packages_clips.php",
			"",
			function (respond) {
				console.log("Packages clips: " + respond);
				if(respond == "error") {
					console.log("Can't fetch packages clips.");
					error = true;
					return;
				}
				try {
					packages_clips = JSON.parse(respond);
					packages_clips_flag = true;
					
				} catch (e) {
					console.log("Can't convert packages clips choices json");
					error = true;
					return;
				}
				update();
			}
		);
	}
	if(!error) {
		ajax_post(
				"/json/clips_choices.php",
				"",
				function (respond) {
					console.log("Clips choices: " + respond);
					if(respond == "error") {
						console.log("Can't fetch clips choices.");
						error = true;
						return;
					}
					try {
						clips_choices = JSON.parse(respond);
						clips_choices_flag = true;
						
					} catch (e) {
						console.log("Can't convert clips choices json");
						error = true;
						return;
					}
					update();
				}
			);
	}

	if(!error) {
		ajax_post(
				"/json/movies.php",
				"",
				function (respond) {
					console.log("Movies: " + respond);
					if(respond == "error") {
						console.log("Can't fetch movies");
						error = true;
						return;
					}
					try {
						movies = JSON.parse(respond);
						movies_flag = true;
						
					} catch (e) {
						console.log("Can't convert movies json e: " );
						error = true;
						return;
					}
					update();
				}
			)
	}

	function update() {
		if(error || !clips_flag || !clips_choices_flag || !movies_flag || !packages_clips_flag) return;
		console.log("---------");
		error = false;
		clips_flag = false;
		clips_choices_flag = false;
		movies_flag = false;
		packages_clips_flag = false;
		/*
			1_ set packaged clips to it's global variable
			2_ set unpackaged clips to it's global variable
			3_ update clips table
			4_ update unpackaged clips label
		*/

		//packaged clips
		//console.log(packages_clips);
		for(var i in packages) {
			packaged_clips[i] = {};
			packaged_clips[i].package_id = packages[i].id;
			var clips_index = 0;
			packaged_clips[i].clips = [];
			for(var c in packages_clips) {
				if(packages[i].id == packages_clips[c].package_id) {
					packaged_clips[i].clips[clips_index] = {
						clip_id: packages_clips[c].clip_id
					}
					clips_index ++;
				}
			}
		}
		console.log("packaged clips: , ps: " + packages.length);
		console.log(packaged_clips);

		//unpackaged clips
		var ind = 0;
		for(var i in clips) {
			if(!find_clip_in_packages_clips(clips[i].id)) {
				unpackaged_clips[ind] = {};
				unpackaged_clips[ind].clip_id = clips[i].id;
				ind ++;
			}
		}
		console.log("unpackaged clips: ");
		console.log(unpackaged_clips);
		update_unpackaged_clips_number();

		//update clips table
		//unpackaged clips
		var table_html = "";
		for(var i in unpackaged_clips) {
			var clip_object = get_clip_object(unpackaged_clips[i].clip_id);
			table_html += "<tr>";
			table_html += "<td style='text-align:center;'>" + clip_object.id + "</td>";
			table_html += "<td style='text-align:center;'>" + clip_object.clip + "</td>";
			table_html += "<td style='text-align:center;'>" + get_movie_name(clip_object.movie_id) + "</td>";
			table_html += "<td style='text-align:center;'>";

			for(var c in clips_choices) {
				if(clips_choices[c].clip_id == clip_object.id) {
					table_html += "(" + get_movie_name(clips_choices[c].movie_id) + ")";
				}
			}
			table_html += "</td>";
			table_html += "<td style='text-align:center;'> <button class='btn-warning btn btn-xs' onclick='delete_clip(" + unpackaged_clips[i].clip_id + ")'> delete </button>";
			table_html += "</td>";
			table_html += "</tr>";
		}

		//packaged clips
		for(var i in packaged_clips) {
			var package_object = get_package_object(packaged_clips[i].package_id);
			
			var __clips = packaged_clips[i].clips;
			//console.log(__clips);

			for(var y in __clips) {
				var clip_object = get_clip_object(__clips[y].clip_id);
				table_html += "<tr bgcolor='" + package_object.color + "'>";
				table_html += "<td style='text-align:center;'>" + clip_object.id + "</td>";
				table_html += "<td style='text-align:center;'>" + clip_object.clip + "</td>";
				table_html += "<td style='text-align:center;'>" + get_movie_name(clip_object.movie_id) + "</td>";
				table_html += "<td style='text-align:center;'>";

				for(var c in clips_choices) {
					if(clips_choices[c].clip_id == clip_object.id) {
						table_html += "(" + get_movie_name(clips_choices[c].movie_id) + ")";
					}
				}
				table_html += "</td>";
				table_html += "<td style='text-align:center;'> <button class='btn btn-xs disabled' disabled> delete </button>";
				table_html += "</tr>";
			}
			
		}
		document.getElementById("clips_table_body").innerHTML = table_html;





		//Update movies table
		var movies_table_html = "";
		for(i in movies) {
			//Movie occurence in answers in clips 
			var ans_count = 0;
			for(c in clips) {
				if(clips[c].movie_id == movies[i].id) ans_count++;
			}
			var choices_count = 0;
			for(c in clips_choices) {
				if(clips_choices[c].movie_id == movies[i].id) choices_count++;
			}
			movies_table_html += "<tr>" +
				"<td style='text-align:center'>" + movies[i].id + "</td>" +
				"<td style='text-align:center'>" + movies[i].name + "</td>" +
				"<td style='text-align:center'>" + ans_count + "</td>" +
				"<td style='text-align:center'>" + choices_count + "</td>" +
				"<td style='text-align:center'><button class='btn btn-xs" + (ans_count + choices_count == 0 ? " btn-warning' onclick='delete_movie("+movies[i].id+")'> ": " disabled'>") + "delete</button></td>" +
				"</tr>";
		}

		document.getElementById("movies_table_body").innerHTML = movies_table_html;

	}
}

function publish_package(id) {
	console.log("publish_package" + id);

	ajax_post(
			"/publish/publish_package.php",
			"id="+id,
			function(respond) {
				console.log(respond);
				if(respond != "done") {
					alert("Server didn't respond with none");
				} else {
					update_packages_list();
				}
			}
		);

}

function delete_package(id) {
	console.log("delete_package" + id);

	ajax_post(
			"/publish/delete_package.php",
			"id="+id,
			function(respond) {
				console.log(respond);
				if(respond != "done") {
					alert("Server didn't respond with done");
				} else {
					update_packages_list();
				}
			}
		);
}

function delete_movie(id) {
	ajax_post(
		"../movies/delete_movie.php",
		"id="+id,
		function(respond) {
			console.log(respond);
			if(respond != "done") {
				alert("Server didn't respond with done");
			}
			update_packages_list();
		}
	)
}


function select_random_clips(n, excluded) {
	if(n > unpackaged_clips.length) return false;
	if(unpackaged_clips.length == n) return unpackaged_clips;

	var arr = new Array();
	var count = 0;
	while(true) {
		var ran_ind = Math.floor(Math.random()*unpackaged_clips.length);

		var clip_chosen = false;
		for(c in arr) {
			if(arr[c] == unpackaged_clips[ran_ind]) clip_chosen = true;
		}
		for(c in excluded) {
			if(arr[c] == excluded[c]) clip_chosen = true;
		}
		count ++;
		if(!clip_chosen)
			arr.push(unpackaged_clips[ran_ind]);
		if(arr.length >= n) break;
	}
	var ans = new Array();
	for(var i=0; i<n ;i++) {
		ans.push(arr[i]);
	}
	console.log(count);
	return ans;
}

function update_unpackaged_clips_number() {
	document.getElementById("unpackaged_clips_number").innerHTML = " Unpackaged clips: " + unpackaged_clips.length;
	document.getElementById("total_packages_number").innerHTML = "(" + packages.length + ")";
	document.getElementById("total_clips_number").innerHTML = "(" + clips.length + ")";
	document.getElementById("movies_number").innerHTML = "(" + movies.length + ")"; 

}

function delete_clip(id) {
	ajax_post(
		"../clips/delete_clip.php",
		"clip_id="+id,
		function(respond){
			console.log(respond);
			update_packages_list();
		}
	);
}

//disabled submit button to avoid conflict in unpackaged clips
function toggle_package_submission() {
	//TODO: toggle submi button
}












//Utilities
function ajax_post(url, data, success_callback) {
	$.ajax({
		type: "POST",
		url: url,
		data: data,
		cache: false,
		success: success_callback
	});
}


function find_clip_in_packages_clips(clip_id) {
	for(var i in packages_clips) {
		if(clip_id == packages_clips[i].clip_id) {
			return true;
		}
	}
	return false;
}

function get_movie_name(movie_id) {
	for(var i in movies) {
		if(movie_id == movies[i].id) {
			return movies[i].name;
		}
	}
	return false;
}

function get_clip_object(clip_id) {
	for(var i in clips) {
		if(clips[i].id == clip_id) {
			return clips[i];
		}
	}
	return false;
}

function get_package_object(package_id) {
	for(var i in packages) {
		if(packages[i].id == package_id) {
			return packages[i];
		}
	}
	return false;
}